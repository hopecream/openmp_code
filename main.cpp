#include <omp.h>
#include <iostream>
using namespace std;

void serial()
{
    for(int i = 0; i < 10; i ++)
        cout << i << endl;
}

void parallel()
{
#pragma omp parallel for
    for(int i = 0; i < 10; i ++)
        cout << i << endl;
}

int main()
{
    cout << "serial output" << endl;
    serial();
    cout << "parallel output" << endl;
    parallel();
    return 0;
}
